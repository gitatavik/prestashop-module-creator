<?php

namespace ModuleCreator\Services;

class CommandService
{
    private const AUTO_INDEX_PATH = "/vendor/bin/autoindex";
    private const HEADER_STAMP_PATH = "/vendor/bin/header-stamp";

    /**
     * Add index.php into all module folders
     *
     * @return false|string
     */
    public function autoIndex()
    {
        $autoindexPath = getcwd() . self::AUTO_INDEX_PATH;
        return exec("{$autoindexPath} 2>&1", $outputArray, $returnVar);
    }

    /**
     * Add stamp comment into each start module files
     *
     * @return false|string
     */
    public function headerStamp()
    {
        $headerStampPath = getcwd() . self::HEADER_STAMP_PATH;
        return exec("{$headerStampPath} --license=assets/afl.txt --exclude=vendor,node_modules 2>&1", $outputArray, $returnVar);
    }
}
