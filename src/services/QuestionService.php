<?php

namespace ModuleCreator\Services;

use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;

class QuestionService
{
    /**
     * @param $helper
     * @param $input
     * @param $output
     * @return mixed
     */
    public function  getDisplayNameQuestion($helper, $input, $output)
    {
        do {
            $displayNameQuestion = new Question('<question>Nom du module en BO</question> ');
            $displayName = $helper->ask($input, $output, $displayNameQuestion);
        } while (empty($displayName));

        return $displayName;
    }

    /**
     * @param $helper
     * @param $input
     * @param $output
     * @return mixed
     */
    public function getDescriptionQuestion($helper, $input, $output)
    {
        do {
            $descriptionQuestion = new Question("<question>Description du module</question> ");
            $description = $helper->ask($input, $output, $descriptionQuestion);
        } while (empty($description));

        return $description;
    }

    /**
     * @param $helper
     * @param $input
     * @param $output
     * @return array
     */
    public function getDependenciesQuestion($helper, $input, $output): array
    {
        $dependencies = [];

        while (true) {
            $dependencyQuestion = new Question("<question>Entrez le nom d'un module dépendant (ou appuyez sur 'Entrée' pour terminer) :</question> ", '');
            $dependencyName = $helper->ask($input, $output, $dependencyQuestion);

            if (empty($dependencyName)) {
                break;
            }

            $dependencies[] = $dependencyName;
        }

        return $dependencies;
    }

    /**
     * @param $helper
     * @param $input
     * @param $output
     * @return mixed
     */
    public function getLogsQuestion($helper, $input, $output)
    {
        $logChoiceQuestion = new ChoiceQuestion(
            "<question>Ajouter le système de logs ? (par défaut à 'Non')</question> ",
            ['Non', 'Oui'],
            0 // Default to 'No'
        );

        $logChoiceQuestion->setErrorMessage('<question>Réponse %s est invalide.</question>');

        return $helper->ask($input, $output, $logChoiceQuestion);
    }

    /**
     * @param $helper
     * @param $input
     * @param $output
     * @return array
     */
    public function getHooksQuestion($helper, $input, $output): array
    {
        $hooks = [];

        while (true) {
            $hookQuestion = new Question("<question>Entrer le nom d'un hook (example: actionValidateOrderAfter) pour se greffer sur celui-ci (ou appuyez sur 'Entrée' pour terminer) :</question> ", '');
            $hookName = $helper->ask($input, $output, $hookQuestion);

            if (empty($hookName)) {
                break;
            }

            $hooks[] = $hookName;
        }

        return $hooks;
    }

    /**
     * @param $helper
     * @param $input
     * @param $output
     * @return mixed
     */
    public function cssQuestion($helper, $input, $output)
    {
        $cssChoiceQuestion = new ChoiceQuestion(
            "<question>Y aura-t-il des fichiers CSS/JS ? (par défaut à 'Non')</question> ",
            ['Non', 'Oui'],
            0 // Default to 'No'
        );

        $cssChoiceQuestion->setErrorMessage('<question>Réponse %s est invalide.</question>');

        return $helper->ask($input, $output, $cssChoiceQuestion);
    }

    /**
     * @param $helper
     * @param $input
     * @param $output
     * @return mixed
     */
    public function tabQuestion($helper, $input, $output)
    {
        $tabChoiceQuestion = new ChoiceQuestion("<question>Y aura-t-il un onglet en Back office ? (par défaut à 'Non')</question> ",
            ['Non', 'Oui'],
            0 // Default to 'No'
        );

        $tabChoiceQuestion->setErrorMessage('<question>Réponse %s est invalide.</question>');

        return $helper->ask($input, $output, $tabChoiceQuestion);
    }

    /**
     * @param $helper
     * @param $input
     * @param $output
     * @return mixed
     */
    public function getContentQuestion($helper, $input, $output)
    {
        $getContentChoiceQuestion = new ChoiceQuestion("<question>Y aura-t-il une méthode getContent ? (par défaut à 'Non')</question> ",
            ['Non', 'Oui'],
            0 // Default to 'Non'
        );

        $getContentChoiceQuestion->setErrorMessage('<question>Réponse %s est invalide.</question>');

        return $helper->ask($input, $output, $getContentChoiceQuestion);
    }

    /**
     * @param $helper
     * @param $input
     * @param $output
     * @return mixed
     */
    public function getTabGetContentQuestion($helper, $input, $output)
    {
        $tabGetContentQuestion = new ChoiceQuestion("<question>Y aura-t-il des onglets dans la méthode getContent ? (par défaut « Oui »)</question> ",
            ['Non', 'Oui'],
            0 // Default to 'Non'
        );

        $tabGetContentQuestion->setErrorMessage('<question>Réponse %s est invalide.</question>');

        return $helper->ask($input, $output, $tabGetContentQuestion);
    }

    /**
     * @param $helper
     * @param $input
     * @param $output
     * @return array
     */
    public function getTabNamesGetContentQuestion($helper, $input, $output): array
    {
        $tabs = [];

        while (true) {
            $getTabNamesGetContentQuestion = new Question("<question>Entrez le nom d'un onglet getContent à ajouter (ou appuyez sur 'Entrée' pour terminer) :</question> ", '');
            $tabNames = $helper->ask($input, $output, $getTabNamesGetContentQuestion);

            if (empty($tabNames)) {
                break;
            }

            $tabs[] = $tabNames;
        }

        return $tabs;
    }

    /**
     * @param $helper
     * @param $input
     * @param $output
     * @return mixed
     */
    public function getControllerQuestion($helper, $input, $output)
    {
        $controllerChoiceQuestion = new ChoiceQuestion("<question>Y aura-t-il des contrôleurs ? (par défaut 'Non')</question> ",
            ['Non', 'Oui'],
            0 // Default to 'No'
        );

        $controllerChoiceQuestion->setErrorMessage('<question>Réponse %s est invalide.</question>');

        return $helper->ask($input, $output, $controllerChoiceQuestion);
    }

    /**
     * @param $helper
     * @param $input
     * @param $output
     * @return mixed
     */
    public function getAnotherControllerQuestion($helper, $input, $output)
    {
        $controllerChoiceQuestion = new ChoiceQuestion("<question>Y aura-t-il un nouveau contrôleur ? (par défaut 'Non')</question> ",
            ['Non', 'Oui'],
            0 // Default to 'No'
        );

        $controllerChoiceQuestion->setErrorMessage('<question>Réponse %s est invalide.</question>');

        return $helper->ask($input, $output, $controllerChoiceQuestion);
    }

    /**
     * @param $helper
     * @param $input
     * @param $output
     * @return mixed
     */
    public function getControllerFrontQuestion($helper, $input, $output)
    {
        $controllerFrontChoiceQuestion = new ChoiceQuestion("<question>Front? (par défaut à 'Non')</question> ",
            ['Non', 'Oui'],
            0 // Default to 'Non'
        );

        $controllerFrontChoiceQuestion->setErrorMessage('<question>Réponse %s est invalide.</question>');

        return $helper->ask($input, $output, $controllerFrontChoiceQuestion);
    }

    /**
     * @param $helper
     * @param $input
     * @param $output
     * @return mixed
     */
    public function getFrontControllerCronQuestion($helper, $input, $output)
    {
        $controllerFrontChoiceQuestion = new ChoiceQuestion("<question>Controller avec cron ? (par défaut à 'Non')</question> ",
            ['Non', 'Oui'],
            0 // Default to 'Non'
        );

        $controllerFrontChoiceQuestion->setErrorMessage('<question>Réponse %s est invalide.</question>');

        return $helper->ask($input, $output, $controllerFrontChoiceQuestion);
    }

    /**
     * @param $helper
     * @param $input
     * @param $output
     * @return mixed
     */
    public function getModuleRouteQuestion($helper, $input, $output)
    {
        $controllerFrontChoiceQuestion = new ChoiceQuestion("<question>Y aura-t-il besoin du hookModuleRoutes pour le controlleur? (par défaut 'Non')</question> ",
            ['Non', 'Oui'],
            0 // Default to 'Non'
        );

        $controllerFrontChoiceQuestion->setErrorMessage('<question>Réponse %s est invalide.</question>');

        return $helper->ask($input, $output, $controllerFrontChoiceQuestion);
    }

    /**
     * @param $helper
     * @param $input
     * @param $output
     * @return mixed
     */
    public function getUrlRewriteQuestion($helper, $input, $output)
    {
        do {
            $frontControllerNameQuestion = new Question("<question>Nom de l'url réécrite pour ce controlleur ?</question> ");
            $frontControllerName = $helper->ask($input, $output, $frontControllerNameQuestion);
        } while (empty($frontControllerName));

        return $frontControllerName;
    }

    /**
     * @param $helper
     * @param $input
     * @param $output
     * @return mixed
     */
    public function getFrontControllerNameQuestion($helper, $input, $output)
    {
        do {
            $frontControllerNameQuestion = new Question("<question>Nom du controlleur Front ?</question> ");
            $frontControllerName = $helper->ask($input, $output, $frontControllerNameQuestion);
        } while (empty($frontControllerName));

        return $frontControllerName;
    }

    /**
     * @param $helper
     * @param $input
     * @param $output
     * @return mixed
     */
    public function getBackControllerNameQuestion($helper, $input, $output)
    {
        do {
            $backControllerNameQuestion = new Question("<question>Nom du controlleur Back ?</question> ");
            $backControllerName = $helper->ask($input, $output, $backControllerNameQuestion);
        } while (empty($backControllerName));

        return $backControllerName;
    }

    /**
     * @param $helper
     * @param $input
     * @param $output
     * @return mixed
     */
    public function getTabChoiceQuestion($helper, $input, $output)
    {
        $tabChoiceQuestion = new ChoiceQuestion("<question>Quel onglet parent pour le controlleur Back ?</question> ", [
            'AdminAtkModuleTabsDiscounts',
            'AdminAtkModuleTabsAsileColis',
            'AdminAtkModuleTabsCustomers',
            'AdminAtkModuleTabsProducts',
            'AdminAtkModuleTabsConfigurationSite'
        ]);

        $tabChoiceQuestion->setErrorMessage('<question>Réponse %s est invalide.</question>');

        return $helper->ask($input, $output, $tabChoiceQuestion);
    }

    /**
     * @param $helper
     * @param $input
     * @param $output
     * @return mixed
     */
    public function getTabNameQuestion($helper, $input, $output)
    {
        do {
            $tabNameQuestion = new Question("<question>Nom de l'onglet Back office (example: Promotions 3+1)?</question> ");
            $tabName = $helper->ask($input, $output, $tabNameQuestion);
        } while (empty($tabName));

        return $tabName;
    }

    /**
     * @param $helper
     * @param $input
     * @param $output
     * @return mixed
     */
    public function getTabClassNameQuestion($helper, $input, $output)
    {
        do {
            $tabClassNameQuestion = new Question("<question>Nom du controlleur Back office (example: AdminCartRulesItem)?</question> ");
            $tabClassName = $helper->ask($input, $output, $tabClassNameQuestion);
        } while (empty($tabClassName));

        return $tabClassName;
    }

    /**
     * @param $helper
     * @param $input
     * @param $output
     * @return mixed
     */
    public function getClassQuestion($helper, $input, $output)
    {
        $classChoiceQuestion = new ChoiceQuestion(
            "<question>Y aura-t-il un ObjectModel ? (par défaut 'Non')</question> ",
            ['Non', 'Oui'],
            0 // Default to 'No'
        );

        $classChoiceQuestion->setErrorMessage('<question>Réponse %s est invalide.</question>');

        return $helper->ask($input, $output, $classChoiceQuestion);
    }

    /**
     * @param $helper
     * @param $input
     * @param $output
     * @return mixed
     */
    public function getAnotherClassQuestion($helper, $input, $output)
    {
        $classChoiceQuestion = new ChoiceQuestion(
            "<question>Y aura-t-il un autre ObjectModel ? (par défaut 'Non')</question> ",
            ['Non', 'Oui'],
            0 // Default to 'No'
        );

        $classChoiceQuestion->setErrorMessage('<question>Réponse %s est invalide.</question>');

        return $helper->ask($input, $output, $classChoiceQuestion);
    }

    /**
     * @param $helper
     * @param $input
     * @param $output
     * @return mixed
     */
    public function getClassNameQuestion($helper, $input, $output)
    {
        do {
            $classNameQuestion = new Question("<question>Quel nom à l'ObjectModel ?</question> ");
            $className = $helper->ask($input, $output, $classNameQuestion);
        } while (empty($className));

        return $className;
    }

    /**
     * @param $helper
     * @param $input
     * @param $output
     * @return mixed
     */
    public function getTemplateQuestion($helper, $input, $output)
    {
        $templateChoiceQuestion = new ChoiceQuestion(
            "<question>Y aura-t-il des templates ? (par défaut à 'Non')</question> ",
            ['Non', 'Oui'],
            0 // Default to 'Non'
        );

        $templateChoiceQuestion->setErrorMessage('<question>Réponse %s est invalide.</question>');

        return $helper->ask($input, $output, $templateChoiceQuestion);
    }

    /**
     * @param $helper
     * @param $input
     * @param $output
     * @return mixed
     */
    public function getAnotherTemplateQuestion($helper, $input, $output)
    {
        $templateChoiceQuestion = new ChoiceQuestion(
            "<question>Y aura-t-il un autre template ? (par défaut à 'Non')</question> ",
            ['Non', 'Oui'],
            0 // Default to 'Non'
        );

        $templateChoiceQuestion->setErrorMessage('<question>Réponse %s est invalide.</question>');

        return $helper->ask($input, $output, $templateChoiceQuestion);
    }

    /**
     * @param $helper
     * @param $input
     * @param $output
     * @return mixed
     */
    public function getTemplateFrontQuestion($helper, $input, $output)
    {
        $templateFrontChoiceQuestion = new ChoiceQuestion(
            "<question>Template Front ? (par défaut 'Non')</question> ",
            ['Non', 'Oui'],
            0 // Default to 'Non'
        );

        $templateFrontChoiceQuestion->setErrorMessage('<question>Réponse %s est invalide.</question>');

        return $helper->ask($input, $output, $templateFrontChoiceQuestion);
    }

    /**
     * @param $helper
     * @param $input
     * @param $output
     * @return mixed
     */
    public function getTemplateNameQuestion($helper, $input, $output)
    {
        do {
            $templateNameQuestion = new Question("<question>Nom du template ?</question> ");
            $templateName = $helper->ask($input, $output, $templateNameQuestion);
        } while (empty($templateName));

        return $templateName;
    }

    /**
     * @param $helper
     * @param $input
     * @param $output
     * @return mixed
     */
    public function getTemplateHookQuestion($helper, $input, $output)
    {
        $templateHookFrontChoiceQuestion = new ChoiceQuestion(
            "<question>Template Hook ? (par défaut 'Non')</question> ",
            ['Non', 'Oui'],
            0 // Default to 'Non'
        );

        $templateHookFrontChoiceQuestion->setErrorMessage('<question>Réponse %s est invalide.</question>');

        return $helper->ask($input, $output, $templateHookFrontChoiceQuestion);
    }

    /**
     * @param $helper
     * @param $input
     * @param $output
     * @return mixed
     */
    public function getTemplateBackQuestion($helper, $input, $output)
    {
        $templateBackChoiceQuestion = new ChoiceQuestion("<question>Template Back ? (par défaut 'Non')</question> ",
            ['Non', 'Oui'],
            0 // Default to 'No'
        );

        $templateBackChoiceQuestion->setErrorMessage('<question>Réponse %s est invalide.</question>');

        return $helper->ask($input, $output, $templateBackChoiceQuestion);
    }

    /**
     * @param $helper
     * @param $input
     * @param $output
     * @return array
     */
    public function getFolderQuestion($helper, $input, $output): array
    {
        $directories = [];

        while (true) {
            $directoryQuestion = new Question("<question>Entrez le nom d'un répertoire à ajouter (ou appuyez sur 'Entrée' pour terminer) :</question> ", '');
            $directoryName = $helper->ask($input, $output, $directoryQuestion);

            if (empty($directoryName)) {
                break;
            }

            $directories[] = $directoryName;
        }

        return $directories;
    }
}
