const webpack = require('webpack');
const path = require('path');
const TerserPlugin = require('terser-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CssMinimizerPlugin = require("css-minimizer-webpack-plugin");

module.exports = (env, argv) => {
    const IS_DEV = argv.mode === "development";
    const IS_PROD = argv.mode === "production";

    return {
        devtool: 'source-map',
        entry: {
            {{ moduleName }}: [
                './js/{{ moduleName }}.js',
                './sass/{{ moduleName }}.scss'
            ],
        },
        output: {
            path: path.resolve(__dirname, '../assets/js'),
            filename: '[name].js',
        },
        resolve: {
            alias: {
                '@img': path.resolve(__dirname, '../assets/img'),
            }
        },
        module: {
            rules: [
                {
                    test: /\.js$/,
                    include: [path.join(__dirname, '')],
                    use: {loader: 'babel-loader'}
                },
                {
                    test: /\.s[ac]ss$/i,
                    use: [
                        {loader: MiniCssExtractPlugin.loader},
                        {
                            loader: 'css-loader',
                            options: {sourceMap: IS_DEV}
                        },
                        {
                            loader: "postcss-loader",
                            options: {
                                postcssOptions: {
                                    plugins: [
                                        [
                                            "postcss-preset-env",
                                            {
                                                // Options
                                            }
                                        ]
                                    ]
                                }
                            }
                        },
                        {
                            loader: 'sass-loader',
                            options: {
                                implementation: require.resolve('sass'),
                                sourceMap: false,
                                sassOptions: {
                                    includePaths: [path.resolve('./node_modules')],
                                    outputStyle: "compressed",
                                }
                            }
                        }
                    ]
                },
                {
                    test: /.(woff(2)?|eot|ttf|svg)(\?[a-z0-9=.]+)?$/,
                    type: 'asset/resource',
                    exclude: /(im(a)?g(e)?)(s\b|\b)/,
                    generator: {filename: '../fonts/[name][ext]'}
                },
                {
                    test: /\.(png|jpe?g|gif|svg|webp)$/,
                    type: 'asset/resource',
                    generator: {filename: '../img/[hash][ext]'}
                },
                {
                    test: /\.css$/i,
                    use: [
                        {loader: 'style-loader'},
                        {loader: 'css-loader', options: {sourceMap: IS_DEV}},
                        {loader: 'postcss-loader', options: {sourceMap: IS_DEV}}
                    ]
                }
            ],
        },
        optimization: {
            minimize: true, // to minimize also in development mode
            minimizer: [
                new CssMinimizerPlugin(),
                new TerserPlugin({
                    parallel: true,
                    test: /\.js(\?.*)?$/i,
                    terserOptions: {
                        compress: {
                            booleans: IS_PROD,
                            conditionals: IS_PROD,
                            //drop_console: IS_PROD,
                            drop_debugger: IS_PROD,
                            if_return: IS_PROD,
                            join_vars: IS_PROD,
                            keep_classnames: IS_DEV,
                            keep_fnames: IS_DEV,
                            reduce_vars: IS_PROD,
                            sequences: IS_PROD,
                            warnings: IS_DEV,
                            ecma: 5,
                        },
                        output: {
                            comments: IS_DEV
                        }
                    }
                }),
            ]
        },
        plugins: [
            new MiniCssExtractPlugin({
                filename: "../css/[name].css",
                chunkFilename: "../css/[id].css"
            }),
            new CssMinimizerPlugin(),
            new webpack.ProvidePlugin({
                Popper: ['popper.js', 'default']
            })
        ],
        watchOptions: {
            ignored: /node_modules/
        },
        externals: {
            prestashop: 'prestashop',
            $: '$',
            jquery: 'jQuery'
        }
    }
}
