<div class="row">
    <div class="col-lg-2">
        <div class="list-group side_menu">
            {foreach from=$forms key='index' item='form'}
                <a href="#{$form.id}" class="list-group-item {if $index eq 0}active{/if}" data-toggle="tab">
                    {$form.label}
                </a>
            {/foreach}
        </div>
    </div>

    <div class="tab-content col-lg-10">
        {foreach from=$forms key='index' item='form'}
            <div id="{$form.id}" class="tab-pane {if $index eq 0}active{/if}">
                {$form.form nofilter}
            </div>
        {/foreach}
    </div>
</div>

<script>
    jQuery('.side_menu a').click(function(){
        var href = jQuery(this).attr('href');
        jQuery('.side_menu a').removeClass('active');
        jQuery(this).addClass('active');
        jQuery('.tab-content .tab-pane').removeClass('active');
        jQuery(href).addClass('active');
    });
</script>
