<?php
class {{ modelName }} extends ObjectModel
{
    /**
     * @var array
     * @see ObjectModel::$definition
     */
     public static $definition = [
        'table' => '{{ tableName }}',
        'primary' => 'id_{{ tableName }}',
        'fields' => []
     ];

    /**
     * @return bool
     */
    public static function installTable(): bool
    {
        return Db::getInstance()->execute(
            'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . self::$definition['table'] . '` (
                `'.self::$definition['primary'].'` INT UNSIGNED NOT NULL AUTO_INCREMENT,
                `date_add` TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
                `date_upd` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                PRIMARY KEY (`'.self::$definition['primary'].'`)
            ) ENGINE = ' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET = utf8;'
        );
    }

    /**
    * @return bool
    */
    public static function uninstallTable(): bool
    {
        return Db::getInstance()->execute('DROP TABLE IF EXISTS  `' . _DB_PREFIX_ . self::$definition['table'] . '`');
    }
}
