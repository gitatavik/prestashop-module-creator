<?php

class {{ moduleName }}{{ controllerName }}ModuleFrontController extends ModuleFrontController
{
    /**
     * @return void
     */
    public function init(): void
    {
        parent::init();
    }

    /**
     * @return void
     */
    public function initContent(): void
    {
        parent::initContent();
    }
}
