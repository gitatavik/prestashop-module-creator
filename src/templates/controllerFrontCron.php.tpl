<?php

class {{ moduleName }}{{ controllerName }}ModuleFrontController extends ModuleFrontController
{
    /**
     * @return void
     */
    public function init(): void
    {
        parent::init();

        if(Tools::getValue('token') == $this->getToken()){
            $this->cronJob();
        }else{
            die('Invalid token');
        }
        die('Finished');
    }

    /**
     * @return string "{{ token }}"
     */
    public function getToken(): string
    {
        return sha1('{{ moduleName }}{{ controllerName }}');
    }

    /**
     * @return void
     */
    public function cronJob(): void
    {
        //TODO à remplir
    }

}
