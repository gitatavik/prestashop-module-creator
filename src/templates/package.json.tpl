{
  "name": "{{ moduleName }}",
  "version": "1.0.0",
  "description": "Tools to help while developing the Atavik theme",
  "author": "Atavik",
  "license": "MIT",
  "scripts": {
    "build": "webpack --mode production",
    "start": "webpack --mode production -w",
    "debug": "webpack --mode development --devtool source-map -w",
    "sass": "webpack --mode development --onlysass true --devtool source-map -w",
    "fulldebug": "webpack --mode development --devtool source-map --output-pathinfo --display-error-details -w --progress --colors"
  },
  "devDependencies": {
    "@babel/core": "^7.24.7",
    "@babel/preset-env": "^7.24.7",
    "babel-loader": "^9.1.3",
    "css-loader": "^7.1.2",
    "css-minimizer-webpack-plugin": "^7.0.0",
    "mini-css-extract-plugin": "^2.9.0",
    "postcss": "^8.4.38",
    "postcss-loader": "^8.1.1",
    "postcss-preset-env": "^9.5.14",
    "sass": "^1.77.4",
    "sass-loader": "^14.2.1",
    "style-loader": "^4.0.0",
    "terser-webpack-plugin": "^5.3.10",
    "webpack": "^5.91.0",
    "webpack-cli": "^5.1.4"
  }
}
