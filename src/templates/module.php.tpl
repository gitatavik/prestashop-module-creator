<?php

if (!defined('_PS_VERSION_')) {
    exit;
}
{{ requireObjectModel }}
class {{ moduleName }} extends Module
{
    {{ logsProperty }}
    public function __construct()
    {
        $this->name = "{{ moduleName }}";
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'Atavik';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = [
            'min' => '1.7',
            'max' => _PS_VERSION_
        ];
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l("{{ displayName }}");
        $this->description = $this->l("{{ description }}");

        {{ controllers }}
        {{ dependencies }}
    }
    {{ logsMethod }}
    /**
     * @return bool
     */
    public function install(): bool
    {
        return parent::install()
            && $this->registerHook([
                {{ displayHeaderHook }}
                {{ hookNames }}
            ])
            {{ tabInstall }}
            {{ installTable }}
        ;
    }

    /**
     * @return bool
     */
    public function uninstall(): bool
    {
        return {{ uninstallTable }} {{ tabUninstall }} parent::uninstall();
    }
    {{ tabMethods }}
    {{ getContent }}
    {{ hookMethods }}
    {{ hookDisplayHeader }}
}
