<?php

namespace ModuleCreator;

use ModuleCreator\Services\CommandService;
use ModuleCreator\Services\QuestionService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ModuleCreator extends Command
{
    protected static $defaultName = 'create-module';
    private $questionService;
    private $commandService;

    /**
     * @param QuestionService $questionService
     * @param CommandService $commandService
     */
    public function __construct(QuestionService $questionService, CommandService $commandService)
    {
        $this->questionService = $questionService;
        $this->commandService = $commandService;

        parent::__construct();
    }

    /**
     * @return void
     */
    protected function configure(): void
    {
        $this
            ->setDescription('Creates a new PrestaShop module.')
            ->setHelp('This command allows you to create a PrestaShop module interactively');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        //$output->getFormatter()->setStyle('question', new OutputFormatterStyle('cyan', null, ['bold']));
        $output->getFormatter()->setStyle('info', new OutputFormatterStyle('green', null, ['bold']));
        $output->getFormatter()->setStyle('error', new OutputFormatterStyle('red', null, ['bold']));

        $moduleDir = getcwd() . '/';
        $helper = $this->getHelper('question');

        // Questions pour l'utilisateur
        $moduleName = basename(getcwd());
        $output->writeln("<info>Module name: {$moduleName}</info>");

        $displayName = $this->questionService->getDisplayNameQuestion($helper, $input, $output);
        $description = $this->questionService->getDescriptionQuestion($helper, $input, $output);
        $dependencies = $this->questionService->getDependenciesQuestion($helper, $input, $output);
        $hooks = $this->questionService->getHooksQuestion($helper, $input, $output);
        $logs = $this->questionService->getLogsQuestion($helper, $input, $output);

        $css = $this->questionService->cssQuestion($helper, $input, $output);
        $getContent = $this->questionService->getContentQuestion($helper, $input, $output);

        $getContentTabNames = [];
        if ($getContent === 'Oui' && $this->questionService->getTabGetContentQuestion($helper, $input, $output) === 'Oui') {
            $getContentTabNames = $this->questionService->getTabNamesGetContentQuestion($helper, $input, $output);
        }

        $frontControllerNames = [];
        $tabs = [];
        $hookModuleRoutes = [];
        // Controller
        $this->questionController($helper, $input, $output, $moduleName, $frontControllerNames, $tabs, $hookModuleRoutes);

        // ObjectModel
        $objectModelNames = [];
        $this->questionObjectModel($helper, $input, $output, $moduleDir, $moduleName, $displayName, $description, $objectModelNames);

        // Templates
        $this->questionTemplate($helper, $input, $output, $moduleDir);

        // Folder
        foreach ($this->questionService->getFolderQuestion($helper, $input, $output) as $directory) {
            $additionalDir = "{$moduleDir}{$directory}";

            if (!file_exists($additionalDir)) {
                mkdir($additionalDir, 0777, true);
                chmod($additionalDir, 0777);
                $output->writeln("Directory created: {$additionalDir}");

            } else {
                $output->writeln("Directory already exists: {$additionalDir}");
            }
        }

        $output->writeln("<info>Module created successfully!</info>");

        // Main module file
        $this->createModuleFiles($moduleName, $displayName, $description, $css === 'Oui', $getContent === 'Oui', $getContentTabNames, $objectModelNames, $tabs, $frontControllerNames, $dependencies, $hooks, $logs === 'Oui', $output, $hookModuleRoutes);

        // Copy image from bundle to module root
        $sourceImagePath = __DIR__ . '/logo.png';
        $targetImagePath = $moduleDir . '/logo.png';

        if (file_exists($sourceImagePath)) {
            if (copy($sourceImagePath, $targetImagePath)) {
                chmod($targetImagePath, 0666);
                $output->writeln("<info>Image copied successfully to: {$targetImagePath}</info>");
            } else {
                $output->writeln("<error>Failed to copy image to: {$targetImagePath}</error>");
            }
        } else {
            $output->writeln("<error>Source image not found: {$sourceImagePath}</error>");
        }

        $this->commandService->autoIndex();
        $this->commandService->headerStamp();

        return Command::SUCCESS;
    }

    /**
     * @param $helper
     * @param $input
     * @param $output
     * @param $moduleName
     * @param array $controllerNames
     * @param bool $anotherController
     */
    private function questionController($helper, $input, $output, $moduleName, &$frontControllerNames, &$tab, &$hookModuleRoutes, $anotherController = false): void
    {
        if ($anotherController) {
            $response = $this->questionService->getAnotherControllerQuestion($helper, $input, $output);
        } else {
            $response = $this->questionService->getControllerQuestion($helper, $input, $output);
        }

        if ($response === 'Oui') {
            // Front
            if ($this->questionService->getControllerFrontQuestion($helper, $input, $output) === 'Oui') {
                $frontControllerName = $this->questionService->getFrontControllerNameQuestion($helper, $input, $output);
                $frontControllerNames[] = $frontControllerName;
                $frontControllerCron = $this->questionService->getFrontControllerCronQuestion($helper, $input, $output);

                if ($this->questionService->getModuleRouteQuestion($helper, $input, $output) === 'Oui') {
                    $hookModuleRoute = $this->questionService->getUrlRewriteQuestion($helper, $input, $output);
                    $hookModuleRoutes[$frontControllerName] = $hookModuleRoute;
                }

                $this->createControllerFiles($moduleName, true, $frontControllerName, $output, $frontControllerCron == 'Oui');
            } else {
                // Back
                $backControllerName = $this->questionService->getBackControllerNameQuestion($helper, $input, $output);
                $this->createControllerFiles($moduleName, false, ucfirst($backControllerName), $output);

                if ($this->questionService->tabQuestion($helper, $input, $output) === 'Oui') {
                    $tab[] = [
                        'class_name' => $this->questionService->getTabClassNameQuestion($helper, $input, $output),
                        'name' => $this->questionService->getTabNameQuestion($helper, $input, $output),
                        'parent_name' => $this->questionService->getTabChoiceQuestion($helper, $input, $output)
                    ];
                }
            }
            $this->questionController($helper, $input, $output, $moduleName, $frontControllerNames, $tab, $hookModuleRoutes, true);
        }
    }

    private function questionObjectModel($helper, $input, $output, $moduleDir, $moduleName, $displayName, $description, &$objectModelNames, $anotherObjectModel = false)
    {
        if ($anotherObjectModel) {
            $response = $this->questionService->getAnotherClassQuestion($helper, $input, $output);
        } else {
            $response = $this->questionService->getClassQuestion($helper, $input, $output);
        }

        if ($response === 'Oui') {
            $objectModelName = $this->questionService->getClassNameQuestion($helper, $input, $output);

            $objectModelNames[] = $objectModelName;

            if (!file_exists("{$moduleDir}classes/")) {
                mkdir("{$moduleDir}classes/", 0777, true);
                $output->writeln("Directory created: {$moduleDir}classes/");
            }

            $this->createFileFromTemplate($output, "{$moduleDir}classes/{$objectModelName}.php", "objectModel.php.tpl", $moduleName, $displayName, $description, $objectModelName);
            $this->questionObjectModel($helper, $input, $output, $moduleDir, $moduleName, $displayName, $description, $objectModelNames, true);
        }
    }

    private function questionTemplate($helper, $input, $output, $moduleDir, $anotherTemplate = false)
    {
        if ($anotherTemplate) {
            $response = $this->questionService->getAnotherTemplateQuestion($helper, $input, $output);
        } else {
            $response = $this->questionService->getTemplateQuestion($helper, $input, $output);
        }

        if ($response === 'Oui') {
            $template = [
                'name' => $this->questionService->getTemplateNameQuestion($helper, $input, $output),
                'front' => $this->questionService->getTemplateFrontQuestion($helper, $input, $output) === 'Oui',
                'back' => false,
                'hook' => $this->questionService->getTemplateHookQuestion($helper, $input, $output) === 'Oui'
            ];

            if (!$template['front']) {
                $template['back'] = $this->questionService->getTemplateBackQuestion($helper, $input, $output) === 'Oui';
            }

            $templateDir = "{$moduleDir}views/templates";

            if ($template['front']) {
                $templateDir .= "/front";
            } elseif ($template['back']) {
                $templateDir .= "/admin";
            }

            if ($template['hook']) {
                $templateDir .= "/hook";
            }

            $template['name'] = strtolower(trim($template['name']));

            if (!file_exists($templateDir)) {
                mkdir($templateDir, 0777, true);
                chmod($templateDir, 0777);
                file_put_contents("{$templateDir}/{$template['name']}.tpl", '');
                $output->writeln("Directory created: {$templateDir}");
            } else {
                $output->writeln("Directory already exists: {$templateDir}");
            }
            $this->questionTemplate($helper, $input, $output, $moduleDir, true);
        }

    }

    /**
     * @param string $moduleName
     * @param string $displayName
     * @param string $description
     * @param bool $withCss
     * @param bool $getContent
     * @param array $getContentTabNames
     * @param string $objectModelName
     * @param array $tab
     * @param array $controllerNames
     * @param array $dependencies
     * @param array $hooks
     * @param bool $logs
     * @param OutputInterface $output
     * @return void
     */
    private function createModuleFiles(
        string          $moduleName,
        string          $displayName,
        string          $description,
        bool            $withCss,
        bool            $getContent,
        array           $getContentTabNames,
        array           $objectModelNames,
        array           $tabs,
        array           $frontControllerNames,
        array           $dependencies,
        array           $hooks,
        bool            $logs,
        OutputInterface $output,
        array           $hookModuleRoutes
    ): void
    {
        $moduleDir = getcwd() . '/';

        // Create base files from templates
        $this->createFileFromTemplate($output, "{$moduleDir}{$moduleName}.php", 'module.php.tpl', $moduleName, $displayName, $description, '', $tabs, $frontControllerNames, $withCss, $dependencies, $hooks, $logs, $getContent, $getContentTabNames, $objectModelNames, $hookModuleRoutes);

        // CSS
        if ($withCss) {
            $devFolder = 'views/_dev';
            $assetFolders = [
                "{$devFolder}/sass",
                "{$devFolder}/js"
            ];

            foreach ($assetFolders as $folder) {
                if (!file_exists("{$moduleDir}{$folder}")) {
                    mkdir("{$moduleDir}{$folder}", 0777, true);
                    $output->writeln("Directory created: {$moduleDir}{$folder}");
                }
            }

            $this->createFileFromTemplate($output, "{$moduleDir}{$devFolder}/webpack.config.js", 'webpack.config.js.tpl', $moduleName);
            $this->createFileFromTemplate($output, "{$moduleDir}{$devFolder}/package.json", 'package.json.tpl', $moduleName);
            $this->createFileFromTemplate($output, "{$moduleDir}{$devFolder}/.browserslistrc", '.browserslistrc.tpl', $moduleName);
            $this->createFileFromTemplate($output, "{$moduleDir}{$devFolder}/babel.config.js", 'babel.config.js.tpl', $moduleName);
            $this->createFileFromTemplate($output, "{$moduleDir}{$devFolder}/sass/{$moduleName}.scss", "styles.scss.tpl", $moduleName);
            $this->createFileFromTemplate($output, "{$moduleDir}{$devFolder}/js/{$moduleName}.js", "styles.js.tpl", $moduleName);
        }
    }

    /**
     * @param string $moduleName
     * @param bool $front
     * @param string $controllerName
     * @param OutputInterface $output
     * @return void
     */
    private function createControllerFiles(
        string          $moduleName,
        bool            $front = true,
        string          $controllerName,
        OutputInterface $output,
        bool            $frontControllerCron = false
    ): void
    {
        $moduleDir = getcwd() . '/';

        $controllerDir = $front ? "{$moduleDir}controllers/front" : "{$moduleDir}controllers/back";

        if (!file_exists($controllerDir)) {
            mkdir($controllerDir, 0777, true);
            $output->writeln("Directory created: {$controllerDir}");
        }

        $templateName = ($front ? ($frontControllerCron ? "controllerFrontCron.php.tpl" : "controllerFront.php.tpl") : "controllerBack.php.tpl");
        $templatePath = __DIR__ . "/templates/{$templateName}";

        $content = file_get_contents($templatePath);
        $content = str_replace('{{ moduleName }}', $moduleName, $content);
        $content = str_replace('{{ controllerName }}', $controllerName, $content);
        $content = str_replace('{{ token }}', sha1($moduleName . $controllerName), $content);

        if (!$front) {
            $controllerName = "Admin{$controllerName}Controller";
        }

        file_put_contents("{$controllerDir}/{$controllerName}.php", $content);
    }

    /**
     * @param string $filePath
     * @param string $templateName
     * @param string $moduleName
     * @param string $displayName
     * @param string $description
     * @param string $objectModelName
     * @param array $tab
     * @param array $controllerNames
     * @param bool $withCss
     * @param array $dependencies
     * @param array $hooks
     * @param bool $logs
     * @param bool $getContent
     * @return void
     */
    private function createFileFromTemplate(
        OutputInterface $output,
        string          $filePath,
        string          $templateName,
        string          $moduleName,
        string          $displayName = '',
        string          $description = '',
        string          $objectModelName = '',
        array           $tabs = [],
        array           $frontControllerNames = [],
        bool            $withCss = false,
        array           $dependencies = [],
        array           $hooks = [],
        bool            $logs = false,
        bool            $getContent = false,
        array           $getContentTabNames = [],
        array           $objectModelNames = [],
        array           $hookModuleRoutes = []
    ): void
    {
        $templatePath = __DIR__ . "/templates/{$templateName}";

        $content = file_get_contents($templatePath);
        $content = str_replace('{{ moduleName }}', $moduleName, $content);
        $content = str_replace("{{ modelName }}", $objectModelName, $content);
        $content = str_replace("{{ tableName }}", strtolower($objectModelName), $content);
        $content = str_replace("{{ displayName }}", $displayName, $content);
        $content = str_replace("{{ description }}", $description, $content);

        // Handle conditional hooks
        $displayHeaderHook = $withCss ? '\'displayHeader\',' : '';
        $hookDisplayHeader = $withCss ? <<<EOT
        
            /**
             * @return void
             */
            public function hookDisplayHeader(): void
            {
                \$this->context->controller->registerStylesheet("modules-{$moduleName}-css", "modules/{$moduleName}/views/assets/css/{$moduleName}.css", ["media" => "all", "priority" => 150]);
                \$this->context->controller->registerJavascript("modules-{$moduleName}-js", "modules/{$moduleName}/views/assets/js/{$moduleName}.js", ["position" => "bottom", "attributes" => "defer", "priority" => 150]);
            } 
        
        EOT: '';

        $content = str_replace('{{ displayHeaderHook }}', $displayHeaderHook, $content);
        $content = str_replace('{{ hookDisplayHeader }}', $hookDisplayHeader, $content);

        //création des onglets dans la fonction getContent
        if ($getContentTabNames) {
            $moduleDir = getcwd() . '/';
            //création du dossier getContent ainsi que le fichier main.tpl
            if (!file_exists("{$moduleDir}views/templates/admin/getContent/")) {
                mkdir("{$moduleDir}views/templates/admin/getContent", 0777, true);
                $output->writeln("Directory created: {$moduleDir}views/templates/admin/getContent/");
                copy(__DIR__ . '/templates/getContent/main.tpl', "{$moduleDir}views/templates/admin/getContent/main.tpl");
            }

            $allTabscontent = <<<EOT
            \$forms = [
            EOT;

            foreach ($getContentTabNames as $tabName) {
                $allTabscontent .= <<<EOT
                        
                            [
                                'id' => Tools::str2url('$tabName'),
                                'label' => '$tabName',
                                'form' => \$this->{'get'.Tools::str2url('$tabName')}(),
                            ],
                EOT;
            }

            $allTabscontent .= <<<EOT
            
                    ];
                    \$this->context->smarty->assign(['forms' => \$forms]);
                    \$html .= \$this->fetch('module:{$moduleName}/views/templates/admin/getContent/main.tpl');
            EOT;
        }

        $getContentMethod = $getContent ? <<<EOT
                
            /**
             * @return string
             */
            public function getContent(): string
            {
                \$html = ''; 
                $allTabscontent
                return \$html;
            }
        
        EOT: '';

        if ($getContentTabNames) {

            foreach ($getContentTabNames as $tabName) {
                $nameFunction = 'get' . urlencode($tabName);
                //création du template associé à la fonction
                if (!file_exists("{$moduleDir}views/templates/admin/getContent/{$nameFunction}.tpl")) {
                    file_put_contents("{$moduleDir}views/templates/admin/getContent/{$nameFunction}.tpl", '');
                    $output->writeln("File created: {$moduleDir}views/templates/admin/getContent/{$nameFunction}.tpl");
                }
                $getContentMethod .= <<<EOT
                
                    /**
                     * @return string
                     */
                    public function {$nameFunction}(): string
                    {
                        \$html = '';
                        //Todo remplir cette fonction
                        return \$this->fetch('module:{$moduleName}/views/templates/admin/getContent/{$nameFunction}.tpl');
                    }
                    
                EOT;
            }
        }

        //Gestion des controllers Front
        if (!empty($frontControllerNames)) {
            $controllersArray = array_map(function ($controller) {
                $controllerNameAbbreviated = strtolower(preg_replace('/Controller$/', '', $controller));
                return "'$controllerNameAbbreviated' => '$controller'";
            }, $frontControllerNames);
            $controllersString = '$this->controllers = [' . implode(', ', $controllersArray) . '];';
            $content = str_replace('{{ controllers }}', $controllersString, $content);
        } else {
            $content = str_replace('{{ controllers }}', '', $content);
        }

        // TODO voir comment faire ici pour mixer avec les autres hooks ? !!! Gestion


        if (!empty($hooks) || $hookModuleRoutes) {
            $hookMethods = '';
            foreach ($hooks as $hook) {
                $hook = ucfirst($hook);
                $hookMethods .= PHP_EOL . "public function hook{$hook}(array \$params) {}" . PHP_EOL;
            }

            if ($hookModuleRoutes) {
                $hooks[] = 'moduleRoutes';

                $hookMethods .= <<<EOT
                  public function hookModuleRoutes() 
                      {
                          return [
                  EOT;

                foreach ($hookModuleRoutes as $controllerName => $urlRewrite) {
                    $hookMethods .= <<<EOT
                    
                                'module-$moduleName-$controllerName' => [
                                    'controller' => '$controllerName',
                                    'rule' => '$urlRewrite',
                                    'keywords' => [],
                                    'params' => [
                                        'fc' => 'module',
                                        'module' => '$moduleName'
                                    ]
                                ],
                    EOT;
                }
                $hookMethods .= <<<EOT
                      
                              ];
                          }
                      EOT;
            }

            $content = str_replace('{{ hookNames }}', implode(',', array_map(function ($hook) {
                return "'$hook'" . PHP_EOL;
            }, $hooks)), $content);
            $content = str_replace('{{ hookMethods }}', $hookMethods, $content);
        } else {
            $content = str_replace('{{ hookNames }}', '', $content);
            $content = str_replace('{{ hookMethods }}', '', $content);
        }

        $content = str_replace("{{ getContent }}", $getContentMethod, $content);

        //Gestion des ObjectModel
        $requireObjectModel = '';
        $installTables = '';
        $uninstallTable = '';
        if ($objectModelNames) {
            $count = 1;
            foreach ($objectModelNames as $objectModelName) {
                $requireObjectModel .= PHP_EOL . "require_once _PS_MODULE_DIR_ . '{$moduleName}/classes/{$objectModelName}.php';" . ($count == count($objectModelNames) ? PHP_EOL : "");
                $installTables .= ($count != 1 ? "            " : "") . "&& {$objectModelName}::installTable()" . ($count == count($objectModelNames) ? "" : PHP_EOL);
                $uninstallTable .= ($count != 1 ? "            " : "") . "{$objectModelName}::uninstallTable() && " . PHP_EOL;
                $count += 1;
            }
        }
        $content = str_replace("{{ requireObjectModel }}", $requireObjectModel ? $requireObjectModel : '', $content);
        $content = str_replace("{{ installTable }}", $installTables ? $installTables : '', $content);
        $content = str_replace("{{ uninstallTable }}", $uninstallTable ? $uninstallTable : '', $content);


        //Gestion des onglet back office
        if ($tabs) {

            $tabMethods = <<<EOT
                /**
                 * @return bool
                 */
                private function installTab(): bool
                {
                
                    \$result = [true];
            EOT;

            foreach ($tabs as $tab) {
                $tabClassName = $tab['class_name'];
                $tabName = $tab['name'];
                $tabParentName = $tab['parent_name'];

                $tabMethods .= <<<EOT
                
                        \$tab = new Tab();
                        \$id_lang = (int) Configuration::get('PS_LANG_DEFAULT');
                        \$tab->name[\$id_lang] = \$this->l("{$tabName}");
                        \$tab->class_name = "{$tabClassName}";
                        \$tab->id_parent = Tab::getIdFromClassName("{$tabParentName}");
                        \$tab->module = \$this->name;
                        \$result[] = \$tab->add();
                    
                EOT;

            }

            $tabMethods .= <<<EOT
                return count(array_unique(\$result)) === 1;
                }
                
            EOT. PHP_EOL;

            $tabMethods .= <<<EOT
                /**
                 * @return bool
                 */
                private function uninstallTab(): bool
                {
                
                    \$result = [true];
            EOT;

            foreach ($tabs as $tab) {
                $tabClassName = $tab['class_name'];
                $tabMethods .= <<<EOT
                
                        \$tab = new Tab((int)Tab::getIdFromClassName("{$tabClassName}"));
                        \$result[] = \$tab->delete();
                    
                EOT;
            }

            $tabMethods .= <<<EOT
            
                    return count(array_unique(\$result)) === 1;
                }
                
            EOT;
        }

        $content = str_replace("{{ tabMethods }}", $tabMethods ?? '', $content);
        $content = str_replace("{{ tabInstall }}", isset($tabMethods) ? "&& \$this->installTab()" : '', $content);
        $content = str_replace("{{ tabUninstall }}", isset($tabMethods) ? "\$this->uninstallTab() && " : '', $content);

        //gestion des Logs
        if ($logs) {
            $logsMethod = <<<EOT
            
               /**
                * @return AtkLogs
                */
                public function getLogger(): AtkLogs
                {
                    Module::getInstanceByName('atk_addlog');
                    \$this->atklogs = new AtkLogs(\$this->name);
            
                    return \$this->atklogs;
                }
                
            EOT;

            $content = str_replace("{{ logsProperty }}", "public \$atklogs;" . PHP_EOL, $content);
            $content = str_replace("{{ logsMethod }}", $logsMethod, $content);

            if (!in_array('atk_addlog', $dependencies)) {
                $dependencies[] = 'atk_addlog';
            }
        } else {
            $content = str_replace("{{ logsProperty }}", '', $content);
            $content = str_replace("{{ logsMethod }}", '', $content);
        }

        //Gestion des dépendances
        if (!empty($dependencies)) {
            $dependenciesString = '$this->dependencies = [' . implode(', ', array_map(function ($dependency) {
                    return "'$dependency'";
                }, $dependencies)) . '];';
            $content = str_replace('{{ dependencies }}', $dependenciesString, $content);
        } else {
            $content = str_replace('{{ dependencies }}', '', $content);
        }

        file_put_contents($filePath, $content);
    }
}
