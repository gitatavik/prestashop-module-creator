<?php

namespace ModuleCreator;

use ModuleCreator\Services\CommandService;
use ModuleCreator\Services\QuestionService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TemplateCreator extends Command
{
    protected static $defaultName = 'create-template';

    private $questionService;
    private $commandService;

    /**
     * @param QuestionService $questionService
     * @param CommandService $commandService
     */
    public function __construct(QuestionService $questionService, CommandService $commandService)
    {
        $this->questionService = $questionService;
        $this->commandService = $commandService;

        parent::__construct();
    }

    /**
     * @return void
     */
    protected function configure(): void
    {
        $this
            ->setDescription('Creates a new PrestaShop module.')
            ->setHelp('This command allows you to create a PrestaShop module interactively');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $moduleDir = getcwd() . '/';
        $helper = $this->getHelper('question');

        $template = [
            'name' => $this->questionService->getTemplateNameQuestion($helper, $input, $output),
            'front' => $this->questionService->getTemplateFrontQuestion($helper, $input, $output) === 'Oui',
            'back' => false,
            'hook' => $this->questionService->getTemplateHookQuestion($helper, $input, $output) === 'Oui'
        ];

        if (!$template['front']) {
            $template['back'] = $this->questionService->getTemplateBackQuestion($helper, $input, $output) === 'Oui';
        }

        $templateDir = "{$moduleDir}views/templates";

        if ($template['front']) {
            $templateDir .= "/front";
        } elseif ($template['back']) {
            $templateDir .= "/admin";
        }

        if ($template['hook']) {
            $templateDir .= "/hook";
        }

        $template['name'] = strtolower(trim($template['name']));

        if (!file_exists($templateDir)) {
            mkdir($templateDir, 0777, true);
            chmod($templateDir, 0777);
            file_put_contents("{$templateDir}/{$template['name']}.tpl", '');
            $output->writeln("Directory created: {$templateDir}");
        } else {
            $output->writeln("Directory already exists: {$templateDir}");
        }

        $this->commandService->autoIndex();
        $this->commandService->headerStamp();

        return Command::SUCCESS;
    }
}
