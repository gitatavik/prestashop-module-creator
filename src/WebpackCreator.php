<?php

namespace ModuleCreator;

use ModuleCreator\Services\CommandService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class WebpackCreator extends Command
{
    protected static $defaultName = 'create-webpack';

    private $commandService;

    /**
     * @param CommandService $commandService
     */
    public function __construct(CommandService $commandService)
    {
        $this->commandService = $commandService;

        parent::__construct();
    }

    /**
     * @return void
     */
    protected function configure(): void
    {
        $this
            ->setDescription('Creates a new PrestaShop module.')
            ->setHelp('This command allows you to create a PrestaShop module interactively');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $moduleName = basename(getcwd());

        $this->createModuleFiles($moduleName, $output);
        $output->writeln('Module created successfully!');

        $this->commandService->autoIndex();
        $this->commandService->headerStamp();

        return Command::SUCCESS;
    }

    /**
     * @param string $moduleName
     * @param OutputInterface $output
     * @return void
     */
    private function createModuleFiles(string $moduleName, OutputInterface $output): void
    {
        $moduleDir = getcwd() . '/';

        // Create webpack.config.js
        $devFolder = 'views/_dev';
        $assetFolders = [
            "{$devFolder}/sass",
            "{$devFolder}/js",
        ];

        foreach ($assetFolders as $folder) {
            if (!file_exists("{$moduleDir}{$folder}")) {
                mkdir("{$moduleDir}{$folder}", 0777, true);
                $output->writeln("Directory created: {$moduleDir}{$folder}");
            }
        }

        $this->createFileFromTemplate("{$moduleDir}{$devFolder}/webpack.config.js", 'webpack.config.js.tpl', $moduleName);
        $this->createFileFromTemplate("{$moduleDir}{$devFolder}/package.json", 'package.json.tpl', $moduleName);
        $this->createFileFromTemplate("{$moduleDir}{$devFolder}/.browserslistrc", '.browserslistrc.tpl', $moduleName);
        $this->createFileFromTemplate("{$moduleDir}{$devFolder}/babel.config.js", 'babel.config.js.tpl', $moduleName);
        $this->createFileFromTemplate("{$moduleDir}{$devFolder}/sass/{$moduleName}.scss", "styles.scss.tpl", $moduleName);
        $this->createFileFromTemplate("{$moduleDir}{$devFolder}/js/{$moduleName}.js", "styles.js.tpl", $moduleName);
    }

    /**
     * @param string $filePath
     * @param string $templateName
     * @param string $moduleName
     * @return void
     */
    private function createFileFromTemplate(
        string $filePath,
        string $templateName,
        string $moduleName
    ): void
    {
        $templatePath = __DIR__ . "/templates/{$templateName}";

        $content = file_get_contents($templatePath);
        $content = str_replace('{{ moduleName }}', $moduleName, $content);

        file_put_contents($filePath, $content);
    }
}
