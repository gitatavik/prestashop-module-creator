<?php

namespace ModuleCreator;

use ModuleCreator\Services\CommandService;
use ModuleCreator\Services\QuestionService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ControllerCreator extends Command
{
    protected static $defaultName = 'create-controller';

    private $questionService;
    private $commandService;

    /**
     * @param QuestionService $questionService
     * @param CommandService $commandService
     */
    public function __construct(QuestionService $questionService, CommandService $commandService)
    {
        $this->questionService = $questionService;
        $this->commandService = $commandService;

        parent::__construct();
    }

    /**
     * @return void
     */
    protected function configure(): void
    {
        $this
            ->setDescription('Creates a new PrestaShop module.')
            ->setHelp('This command allows you to create a PrestaShop module interactively');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $moduleName = basename(getcwd());
        $helper = $this->getHelper('question');

        // Front
        if($this->questionService->getControllerFrontQuestion($helper, $input, $output) === 'Oui') {
            $frontControllerName = $this->questionService->getFrontControllerNameQuestion($helper, $input, $output);
            $frontControllerCron = $this->questionService->getFrontControllerCronQuestion($helper, $input, $output);

            $this->createControllerFiles($moduleName, true, $frontControllerName, $output, $frontControllerCron == 'Oui');
        } else {
            // Back
            $backControllerName = $this->questionService->getBackControllerNameQuestion($helper, $input, $output);
            $this->createControllerFiles($moduleName, false, ucfirst($backControllerName), $output);
        }

        $this->commandService->autoIndex();
        $this->commandService->headerStamp();

        return Command::SUCCESS;
    }

    /**
     * @param string $moduleName
     * @param bool $front
     * @param string $controllerName
     * @param OutputInterface $output
     * @return void
     */
    private function createControllerFiles(
        string          $moduleName,
        bool            $front = true,
        string          $controllerName,
        OutputInterface $output,
        bool            $frontControllerCron = false
    ): void
    {
        $moduleDir = getcwd() . '/';

        $controllerDir = $front ? "{$moduleDir}controllers/front" : "{$moduleDir}controllers/back";

        if (!file_exists($controllerDir)) {
            mkdir($controllerDir, 0777, true);
            $output->writeln("Directory created: {$controllerDir}");
        }

        $templateName = ($front ? ($frontControllerCron ? "controllerFrontCron.php.tpl" : "controllerFront.php.tpl") : "controllerBack.php.tpl");
        $templatePath = __DIR__ . "/templates/{$templateName}";

        $content = file_get_contents($templatePath);
        $content = str_replace('{{ moduleName }}', $moduleName, $content);
        $content = str_replace('{{ controllerName }}', $controllerName, $content);
        $content = str_replace('{{ token }}', sha1($moduleName.$controllerName), $content);

        if(!$front) {
            $controllerName = "Admin{$controllerName}Controller";
        }

        file_put_contents("{$controllerDir}/{$controllerName}.php", $content);
    }
}
