# Atavik Prestashop Module Creator

PrestaShop Module Creator est un bundle Composer permettant de créer des modules PrestaShop de manière interactive et rapide.

## Installation

Via composer :

```bash
$ cd 'your-module'
$ composer require --dev atavik/prestashop-module-creator
```

## Commandes

### Create module

Initialisation complète de votre module prestashop avec cette commande.

```bash
$ vendor/bin/create-module
```

### Create Webpack

Si vous êtes sur un module déjà existant, vous pouvez lancer cette commande pour créer une structure de compilation.

```bash
$ vendor/bin/create-webpack
```

### Create Template

Pour créer une structure de template (front/back/hook) et générer des fichiers .tpl

```bash
$ vendor/bin/create-template
```

### Create Controller

Pour créer une structure de controllers et générer des fichiers .php

```bash
$ vendor/bin/create-controller
```

## Licence

Ce projet est sous licence [Academic Free License 3.0](LICENSE.md).

## Auteurs

[Atavik](https://www.atavik.fr) et Contributeurs - Développement initial - PrestaShop
